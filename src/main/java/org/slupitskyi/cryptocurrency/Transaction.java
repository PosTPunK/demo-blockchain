package org.slupitskyi.cryptocurrency;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slupitskyi.blockchain.Blockchain;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class Transaction {

    private String transactionId;
    private PublicKey sender;
    private PublicKey receiver;
    private double amount;
    private byte[] signature;

    private List<TransactionInput> inputs;
    private List<TransactionOutput> outputs;

    public Transaction(PublicKey sender, PublicKey receiver, double amount,
                       List<TransactionInput> inputs) {
        this.outputs = new ArrayList<>();
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.inputs = inputs;
        calculateHash();
    }

    private void calculateHash() {
        String hashData = sender.toString() + receiver.toString() + amount;
        this.transactionId = CryptographyHelper.generateHash(hashData);
    }

    public void generateSignature(PrivateKey privateKey) {
        String data = sender.toString() + receiver.toString() + amount;
        signature = CryptographyHelper.sign(privateKey, data);
    }

    public boolean verifySignature() {
        String data = sender.toString() + receiver.toString() + amount;
        return CryptographyHelper.verify(sender, data, signature);
    }

    private double getInputsSum() {
        double sum = 0;
        for (TransactionInput transactionInput : inputs){
            if (transactionInput.getUTXO() != null){
                sum += transactionInput.getUTXO().getAmount();
            }
        }
        return sum;
    }

    public boolean verifyTransaction() {

        if(!verifySignature()) {
            System.out.println("Invalid transaction because of invalid signature...");
            return false;
        }

        // let's gather unspent transactions (we have to consider the inputs)
        for(TransactionInput transactionInput : inputs){
            transactionInput.setUTXO(Blockchain.UTXOs.get(transactionInput.getTransactionOutputId()));
        }

        //transactions have 2 part: send an amount to the receiver + send the (balance-amount)
        // back to the sender
        //send value to recipient
        outputs.add(new TransactionOutput(transactionId, this.receiver, amount));
        //send the left over 'change' back to sender
        outputs.add(new TransactionOutput(transactionId, this.sender, getInputsSum() - amount));

        // WE HAVE TO UPDATE THE UTXOs
        //the outputs will be inputs for other transactions (so put them in the blockchain's UTXOs)
        for(TransactionOutput transactionOutput : outputs){
            Blockchain.UTXOs.put(transactionOutput.getId() , transactionOutput);
        }

        // remove transaction inputs from blockchain's UTXOs list because they've been spent
        for(TransactionInput transactionInput : inputs){
            if(transactionInput.getUTXO() != null) {
                Blockchain.UTXOs.remove(transactionInput.getUTXO().getId());
            }
        }
        return true;
    }


}
