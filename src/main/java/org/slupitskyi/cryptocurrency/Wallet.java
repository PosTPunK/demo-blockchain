package org.slupitskyi.cryptocurrency;

import lombok.Getter;
import org.slupitskyi.blockchain.Blockchain;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
public class Wallet {

    private final PublicKey publicKey;
    private final PrivateKey privateKey;


    public Wallet() {
        KeyPair keyPair = CryptographyHelper.ellipticCurveCrypto();
        this.publicKey = keyPair.getPublic();
        this.privateKey = keyPair.getPrivate();
    }

    // there is no balance associated with the users
    // UTXOs and consider all the transactions in the past
    public double calculateBalance() {

        double balance = 0;

        for (Map.Entry<String, TransactionOutput> item: Blockchain.UTXOs.entrySet()) {
            TransactionOutput transactionOutput = item.getValue();

            if(transactionOutput.isMine(publicKey)){
                balance += transactionOutput.getAmount() ;
            }
        }

        return balance;
    }

    // WE ARE ABLE TO TRANSFER MONEY !!!
    // miners of the blockchain will put this transaction into the blockchain
    public Transaction transferMoney(PublicKey receiver, double amount) {

        if(calculateBalance() < amount) {
            System.out.println("Invalid transaction because of not enough money...");
            return null;
        }

        //we store the inputs for the transaction in this array
        List<TransactionInput> inputs = new ArrayList<>();

        // let's find our unspent transactions (the blockchain stores all the UTXOs)
        for (Map.Entry<String, TransactionOutput> item: Blockchain.UTXOs.entrySet()) {
            TransactionOutput UTXO = item.getValue();

            if(UTXO.isMine(publicKey)){
                inputs.add(new TransactionInput(UTXO.getId()));
            }
        }

        //let's create the new transaction
        Transaction newTransaction = new Transaction(publicKey, receiver, amount, inputs);
        //the sender signs the transaction
        newTransaction.generateSignature(privateKey);

        return newTransaction;
    }
}
