package org.slupitskyi.cryptocurrency;

import lombok.SneakyThrows;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.ECGenParameterSpec;

public class CryptographyHelper {

    private CryptographyHelper(){
        throw new IllegalStateException("Utility class");
    }

    @SneakyThrows
    public static String generateHash(String data){
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte [] hash = digest.digest(data.getBytes(StandardCharsets.UTF_8));
        StringBuilder hexademicalString = new StringBuilder();

        for (byte b: hash){
            String hexademical = Integer.toHexString(0xff & b);
            if (hexademical.length() == 1){
                hexademicalString.append("0");
            }
            hexademicalString.append(hexademical);
        }
        return hexademicalString.toString();
    }

    @SneakyThrows
    public static KeyPair ellipticCurveCrypto(){
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("ECDSA", "BC");
        ECGenParameterSpec params = new ECGenParameterSpec("prime256v1");
        keyPairGenerator.initialize(params);
        return keyPairGenerator.generateKeyPair();
    }

    @SneakyThrows
    public static byte[] sign(PrivateKey privateKey, String data){
        Signature signature = Signature.getInstance("ECDSA", "BC");
        signature.initSign(privateKey);
        signature.update(data.getBytes());
        return signature.sign();
    }

    @SneakyThrows
    public static boolean verify(PublicKey publicKey, String data, byte[] sign){
        Signature signature = Signature.getInstance("ECDSA", "BC");
        signature.initVerify(publicKey);
        signature.update(data.getBytes());
        return signature.verify(sign);
    }

}
