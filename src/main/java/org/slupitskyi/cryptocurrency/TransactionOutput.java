package org.slupitskyi.cryptocurrency;

import lombok.Getter;
import lombok.ToString;

import java.security.PublicKey;

@Getter
@ToString
public class TransactionOutput {

    private String id;
    private String parentTransactionId;
    private PublicKey receiver;
    private double amount;

    public TransactionOutput(String parentTransactionId, PublicKey receiver, double amount) {
        this.parentTransactionId = parentTransactionId;
        this.receiver = receiver;
        this.amount = amount;
        generateId();
    }

    private void generateId() {
        this.id = CryptographyHelper.generateHash(receiver.toString() + amount + parentTransactionId);
    }

    public boolean isMine(PublicKey publicKey) {
        return publicKey == receiver;
    }

}
