package org.slupitskyi.cryptocurrency;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionInput {

    private String transactionOutputId;

    private TransactionOutput UTXO;

    public TransactionInput(String transactionOutputId) {
        this.transactionOutputId = transactionOutputId;
    }

}
