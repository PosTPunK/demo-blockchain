package org.slupitskyi.blockchain;

import lombok.Getter;
import lombok.ToString;
import org.slupitskyi.constants.Constants;
import org.slupitskyi.cryptocurrency.CryptographyHelper;
import org.slupitskyi.cryptocurrency.Transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@ToString
public class Block {
    private final long timestamp;
    private final String previousHash;
    private final List<Transaction> transactions = new ArrayList<>();
    private int nonce;
    private String hash;

    public Block(String previousHash){
        this.previousHash = previousHash;
        this.timestamp = new Date().getTime();
        generateHash();
    }

    public void generateHash() {
        String dataToHash = previousHash
                + timestamp
                + transactions
                + nonce;
        this.hash = CryptographyHelper.generateHash(dataToHash);
    }

    public void incrementNonce(){
        this.nonce++;
    }

    public void addTransaction(Transaction transaction) {

        if(transaction == null) return;

        // if the block is the genesis block we do not process
        if((!previousHash.equals(Constants.GENESIS_BLOCK))) {
            if((!transaction.verifyTransaction())) {
                System.out.println("Transaction is not valid...");
                return;
            }
        }

        transactions.add(transaction);
        System.out.println("Transaction is valid and it's added to the block " + this);
    }
}
