package org.slupitskyi.blockchain;

import lombok.Getter;
import org.slupitskyi.cryptocurrency.TransactionOutput;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Getter
public class Blockchain {

    private final List<Block> chain;

    public static final Map<String, TransactionOutput> UTXOs = new HashMap<>();

    public Blockchain(){
        this.chain = new LinkedList<>();
    }

    public void addBlock(Block block){
        this.chain.add(block);
    }

    public int getSize(){
        return this.chain.size();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Block block: this.chain){
            s.append(block.toString());
        }
        return s.toString();
    }
}
